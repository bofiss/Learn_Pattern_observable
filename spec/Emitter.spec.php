<?php
use Event\Emitter;
use Kahlan\plugin\Double;
describe(Emitter::class, function() {

// génerer des test unitaire avec des singletons
   beforeEach(function () {
       $reflection = new ReflectionClass(Emitter::class);
       $instance = $reflection->getProperty('_instance');
       $instance->setAccessible(true);
       $instance->setValue(null, null);
       $instance->setAccessible(false);
   });
   
   given('emitter', function () {
       return Emitter::getInstance();
   });

    
    it('should be singleton', function() {
        expect($this->emitter)->toBeAnInstanceOf(Emitter::class);
        expect($this->emitter)->toBe(Emitter::getInstance() );
    });

    describe('::on', function() {
        it('should trigger the listener event' , function() {
            $listener = Double::instance();
            $comment = ['Belle', 'John'];
          
            // expect($listener)->toReceive('onNewComment')->once()->with($comment);
            expect($listener)->toReceive('onNewComment')->times(1)->with($comment);
            $this->emitter->on('Comment.created', [$listener, 'onNewComment']);
            $this->emitter->on('demo', function (){
                echo 'salut !';
            });

            $this->emitter->emit('Comment.created', $comment);
           
        });

       it ('should trigger events in the right order', function () {

            $listener = Double::instance();
            expect($listener)->toReceive('onNewComment2')->once()->ordered;
            expect($listener)->toReceive('onNewComment')->once()->ordered;

            $this->emitter->on('Comment.created', [$listener, 'onNewComment'], 1);
            $this->emitter->on('Comment.created', [$listener, 'onNewComment2'], 200);

            $this->emitter->emit('Comment.created');

       });

    });
});