<?php
namespace Event;

class Listener {
/**
 * 
 * @var $callable
 */
private  $callable;

/**
 * 
 * @var $priority
 */
private $priority;

 /**
  * initialisation du listener
  * 
  * @param callable $callable
  * @param int $priority
  */
 public function __construct(callable $callable, int $priority){
    $this->callable = $callable;
    $this->priority = $priority;
 }

 public function handle($args){
     call_user_func_array($this->callable, $args);
 }



}