<?php

namespace Event;

class Emitter {

    private static $_instance;
    /**
     * Enregistre la liste des écouteurs
     * 
     * @var Listener[][]
     */
    private $listeners = [];
    
    /**
     * Permet de recuperer l'instance ds l'émetteur
     * 
     * @return Emitter
     */

    public static function getInstance () : Emitter {

        if (!self::$_instance) {
            self::$_instance = new self();
        } 

        return self::$_instance;
    }

   /**
     * Envoie un évènement
     * 
     * @param string $event Nom de l'évènement'
     * @param array ...args
     * @return void
     */
    public function emit(string $event, ...$args) {
        
        if ($this->hasListener($event)) {

            foreach($this->listeners[$event] as $listener){
                $listener->handle($args);
            }
        }
    }
    
    /**
     * Permet d'écouter un évènement
     * 
     * @param string $event Nom de l'évènement'
     * @param callable $callable
     * @param int $priority
     * @return Listener
     */
    public function on(string $event, callable $callable, int $priority = 0): Listener {

        if (!$this->hasListener($event)) {
            $this->listeners[$event] = [];
        }
        
        $listener = new Listener($callable, $priority);
        $this->listeners[$event][] = $listener;
        $this->sortListeners($event);
        return $listener;
    }
    
    /**
     * vérifie si l'évènement est écouté.'
     * 
     * @param $event
     * @return bool
     */
    private function hasListener($event):bool {
        return array_key_exists($event, $this->listeners);
    }

   /**
    * trie le tableau des évènements suivant la priorité.
    * 
    * @param $event
    * @return bool
    */
   private function sortListeners($event): bool {

       uasort($this->listeners[$event], function ($a, $b) {
           return $a->priority < $b-> priority;
       });
   }

}