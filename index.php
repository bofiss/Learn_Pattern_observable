<?php 

require 'vendor/autoload.php';

$emitter = \Event\Emitter::getInstance();

$emitter->on('Comment.created', function($nom, $prenom){
    echo $nom. ' '. $prenom . ' à posté un nouveau commentaire';
});




$emitter->emit('Comment.created', 'Belle', 'Victor');
$emitter->emit('Comment.created', 'Belle', 'Victor');
$emitter->emit('Comment.created', 'Belle', 'Victor');
$emitter->emit('Comment.created', 'Belle', 'Victor');
$emitter->emit('User.new');